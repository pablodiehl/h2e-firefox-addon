/* 2017 - Pablo Diehl ~ www.pablodiehl.xyz | www.gitlab.com/pablodiehl */

//Swaps the visibility from first div to second div
function firstToSecond() {
    'use strict';
    document.getElementById('first').style.display = 'none';
    document.getElementById('second').style.display = 'block';
}

//Swaps the visibility from second div to first div
function secondToFirst() {
    'use strict';
    document.getElementById('second').style.display = 'none';
    document.getElementById('first').style.display = 'block';
}

//Only compress the Input
function justCompress() {
    'use strict';
    var txt = document.getElementById('input').value;
    txt = html2embed(txt, false);
    firstToSecond();
    document.getElementById('result').value = txt;
}

//Compress the input and prepare it to be embed
function embedCompress() {
    'use strict';
    var txt = document.getElementById('input').value;
    txt = html2embed(txt, false, true);
    firstToSecond();
    document.getElementById('result').value = txt;
}

//Downloads the compressed input into a text file
function download() {
    'use strict';
    var file = window.document.createElement('a');
    file.href = window.URL.createObjectURL(new Blob([document.getElementById('result').value], {type: 'text'}));
    file.download = 'h2e.txt';
    document.body.appendChild(file);
    file.click();
    document.body.removeChild(file);
}

//Clear the output
function clear() {
    'use strict';
    document.getElementById('result').value = '';
}


document.getElementById('just-compress').addEventListener("click", (e) => {
    justCompress();
});

document.getElementById('embed-compress').addEventListener("click", (e) => {
    embedCompress();
});

document.getElementById('back').addEventListener("click", (e) => {
    secondToFirst();
});

document.getElementById('download').addEventListener("click", (e) => {
    download();
});

document.addEventListener('DOMContentLoaded', function (event) {
    'use strict';
    new Clipboard('#copy');
});
